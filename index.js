/*
* Formating Namespace
*
* At Udacity, our products have a worldwide audience, so we often
* have to deal with varied input data and cultural norms. One example
* of this is formatting names for display.
*
* In many East Asian languages, namely Chinese, Japanese and
* Korean (CJK), the family name (surname) comes before the given name
* and the two are not separated by a space (ex. "李晓东", where "李" is
* the surname and "晓东" is the given name). In many cultures which
* use the latin alphabet, a person may have one or more given names,
* which proceed the surname and are separated by spaves (ex. "Eric
* Louis Morris", where "Eric" and "Louis" are two given names and
* "Morris" is the surname).
*
* For this exercise, we would like you to do three things:
*
* 1) Write the formatName() function to take each part of the user’s
*    name as an argument and format it apropriately. The input should
*    be in the order: primary given name, additional given name(s)
*    (optional), surname.
* 2) Write the formatInitials() function to:
*    a) Return the capitalized first character of each name if the name
*       consists of Latin (non-CJK) characters.
*    b) Return the full family name if the name consists of CJK characters
* 3) Add at least 3 tests to account for additional edge cases
*
*/

/* East Asian scripts according to https://www.unicode.org/versions/Unicode10.0.0/ch18.pdf */
const scripts = [];
scripts.push({
  name: 'CJK Unified Ideographs',
  range: [parseInt('4e00', 16), parseInt('9fff', 16)]
});
scripts.push({
  name: 'CJK Unified Ideographs Extension A',
  range: [parseInt('3400', 16), parseInt('4dbf', 16)]
});
scripts.push({
  name: 'CJK Unified Ideographs Extension B',
  range: [parseInt('20000', 16), parseInt('2a6df', 16)]
});
scripts.push({
  name: 'CJK Unified Ideographs Extension C',
  range: [parseInt('2a700', 16), parseInt('2b73f', 16)]
});
scripts.push({
  name: 'CJK Unified Ideographs Extension D',
  range: [parseInt('2b740', 16), parseInt('2b81f', 16)]
});
scripts.push({
  name: 'CJK Unified Ideographs Extension E',
  range: [parseInt('2b820', 16), parseInt('2ceaf', 16)]
});
scripts.push({
  name: 'CJK Unified Ideographs Extension F',
  range: [parseInt('2ceb0', 16), parseInt('2ebe0', 16)]
});
scripts.push({
  name: 'CJK Compatibility Ideographs',
  range: [parseInt('f900', 16), parseInt('faff', 16)]
});
scripts.push({
  name: 'Hangul Syllables',
  range: [parseInt('ac00', 16), parseInt('d7a3', 16)]
});
scripts.push({
  name: 'Hiragana',
  range: [parseInt('3040', 16), parseInt('309f', 16)]
});
scripts.push({
  name: 'Katakana',
  range: [parseInt('30a0', 16), parseInt('30ff', 16)]
});
scripts.push({
  name: 'Katakana Phonetic Extensions',
  range: [parseInt('31f0', 16), parseInt('31ff', 16)]
});
scripts.push({
  name: 'Yi',
  range: [parseInt('a000', 16), parseInt('a4cf', 16)]
});
scripts.push({
  name: 'Nüshu',
  range: [parseInt('1b170', 16), parseInt('1b2ff', 16)]
});
scripts.push({
  name: 'Lisu',
  range: [parseInt('a4d0', 16), parseInt('a4ff', 16)]
});
scripts.push({
  name: 'Miao',
  range: [parseInt('16f00', 16), parseInt('16f9f', 16)]
});
scripts.push({
  name: 'CJK Compatibility Ideographs Supplement',
  range: [parseInt('2f800', 16), parseInt('2fa1f', 16)]
});
scripts.push({
  name: 'Kana Supplement',
  range: [parseInt('1b000', 16), parseInt('1b0ff', 16)]
});
scripts.push({
  name: 'Kana Extended-A',
  range: [parseInt('1b100', 16), parseInt('1b12f', 16)]
});
scripts.push({
  name: 'Hangul Jamo',
  range: [parseInt('1100', 16), parseInt('11ff', 16)]
});
scripts.push({
  name: 'Hangul Jamo Extended-A',
  range: [parseInt('a960', 16), parseInt('a97f', 16)]
});
scripts.push({
  name: 'Hangul Jamo Extended-B',
  range: [parseInt('d7b0', 16), parseInt('d7ff', 16)]
});
scripts.push({
  name: 'Hangul Compatibility Jamo',
  range: [parseInt('3130', 16), parseInt('318f', 16)]
});
scripts.push({
  name: 'Tangut',
  range: [parseInt('17000', 16), parseInt('187ff', 16)]
});

function charCode(s) {
  const c = s.charCodeAt(0);

  return {
    inRange: function(range) {
      return c >= range[0] && c <= range[1];
    }
  };
}

function isEastAsianChar(s) {
  return scripts.some(
    script => charCode(s).inRange(script.range)
  );
}

function formatEastAsianName(...names) {
  return names.splice(names.length - 1, 1).concat(names).join('');
}

function formatNonEastAsianName(...names) {
  return names.join(' ');
}

function formatName(...names) {
  return isEastAsianChar(names[names.length - 1]) ? formatEastAsianName(...names) : formatNonEastAsianName(...names);
}

function initials(...names) {
  return names.map(n => n[0].toUpperCase()).join('');
}

function formatInitials(...names) {
  return isEastAsianChar(names[names.length - 1]) ? names[names.length - 1] : initials(...names);
}

/*
* Testing Code
*/

function test(received, expected) {
  if (received === expected) {
    console.log(`[*] Test Passed - ${expected}`);
    return;
  }
  console.log(`[ ] Expected "${expected}", but received "${received}"`);
}

console.log('\nFormats east asian names and non east asian names correctly');
test(formatName('ᎠᎲᏳᏐ', 'ᏊᎺᏭ'), 'ᎠᎲᏳᏐ ᏊᎺᏭ');
test(formatName('譲', '久石'), '久石譲');
test(formatName('도', '이'), '이도');
test(formatName('祹', '李'), '李祹');
test(formatName('もりへい', 'うえしば'), 'うえしばもりへい');
test(formatName('耳', '李'), '李耳');
test(formatName('Ěr', '李'), '李Ěr');
test(formatName('Ěr', 'Ěr'), 'Ěr Ěr');
test(formatName('ლაზური', 'ნენა'), 'ლაზური ნენა');
test(formatName('Δημήτρης', 'Χριστούλιας'), 'Δημήτρης Χριστούλιας');
test(formatName('ẩ', 'ệ', 'ỡ'), 'ẩ ệ ỡ');

console.log('\nFormats east asian initials (family name) and non east asian initials correctly');
test(formatInitials('ẩ', 'ệ', 'ỡ'), 'ẨỆỠ');
test(formatInitials('もりへい', 'うえしば'), 'うえしば');
test(formatInitials('譲', '久石'), '久石');
test(formatInitials('Joe', '久石'), '久石');
test(formatInitials('Ěr', 'Ěr'), 'ĚĚ');
test(formatInitials('Ěr', '李'), '李');

console.log('\nFormats the given name and surname correctly');
test(formatName('Eric', 'Morris'), 'Eric Morris');
test(formatName('晓东', '李'), '李晓东');

console.log('\nHandles an optional middle name');
test(formatName('Eric', 'Louis', 'Morris'), 'Eric Louis Morris');

console.log('\nOutputs a single initial for CJK and all for non-CJK');
test(formatInitials('Eric', 'Morris'), 'EM');
test(formatInitials('Eric', 'Louis', 'Morris'), 'ELM');
test(formatInitials('晓东', '李'), '李');
